import { Injectable } from '@angular/core';
import { Http }       from '@angular/http';
import {AngularFire} from 'angularfire2'; 
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';

@Injectable()
export class ProductsService {

  productsObservable;

  constructor(private af:AngularFire) { }
  
//getProducts(){
//this.productsObservable = this.af.database.list('/products');

  //  return this. productsObservable;
	//}

    deleteProduct1(product){
    this.af.database.object('/products/' + product.$key).remove();
    console.log('/product/' + product.$key);
  }

  addProduct1 (product){
 this.productsObservable.push(product);   
 }

 updateProduct(product){
   let productKey = product.$key;
    let product1 = {categoryId:product.categoryId,cnamme:product.cnamme,cost:product.cost,pid:product.pid}
    console.log(product1);
    this.af.database.object('/products/' + productKey).update(product1)
  }

 getProducts(){
    this.productsObservable = this.af.database.list('/products').map(
      products => {
        products.map(
          product => {
          product.categoryNames =[];
            product.categoryNames.push(this.af.database.object('/category/' + product.categoryId)
            )
          }
        
    );
    return products;
  }
    )
    return this.productsObservable;
	}


}
