import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {NgForm} from '@angular/forms';
import {Product} from '../product/product'

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.css']
})
export class ProductFormComponent implements OnInit {

   @Output() productAddedEvent = new EventEmitter<Product>();
    product:Product = {
     categoryId: '',
     cnamme: '',
     cost: '',
     pid: ''
    


  };

  constructor() { }

  onSubmit(form:NgForm){
    console.log(form);
    this.productAddedEvent.emit(this.product);
    this.product = {
     categoryId: '',
     cnamme: '',
     cost: '',
     pid: ''
    
    }
  }

  ngOnInit() {
  }

}
