import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {User} from './user'

@Component({
  selector: 'jce-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  inputs:['user']
})
export class UserComponent implements OnInit {

  @Output() deleteEvent = new EventEmitter<User>();
  @Output() editEvent = new EventEmitter<User>(); 
  
  user:User;
  isEdit : boolean = false;
  editButtonText = 'Edit';
  constructor() { }
  sendDelete(){
     this.deleteEvent.emit(this.user);
    }
  
  toggleEdit(){
    //update parent about the change
   this.isEdit = !this.isEdit; 
    this.isEdit ?  this.editButtonText = 'Save' : this.editButtonText = 'Edit'; 
    if(!this.isEdit){
    this.editEvent.emit(this.user);
  }   

  }
  ngOnInit() {
  }

}