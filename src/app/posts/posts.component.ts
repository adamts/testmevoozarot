import { Component, OnInit } from '@angular/core';
import {PostsService} from './posts.service';

@Component({
  selector: 'jce-posts',
  templateUrl: './posts.component.html',
  styles: [`
    .posts li { cursor: default; }
    .posts li:hover { background: #ecf0f1; }
    .list-group-item.active, 
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
    }     
  `]
})
export class PostsComponent implements OnInit {

 posts;

 currentPost;

 isLoading = true;
 select(post){
		this.currentPost = post; 
    console.log(	this.currentPost);
 }

  constructor(private _postsService:PostsService) { }

   addPost(post){
       this. _postsService.addPost(post);


   }

   deletePost(post){
   //this.users.splice(    // we delete this because we want delete from firebase 
   //  this.users.indexOf(user),1 // indexOf(user) is the location and 1 is the quantity
  //  )
 
  this._postsService.deletePost(post);
   }

   updatePost(post){
  this._postsService.updatePost(post);

}





  ngOnInit() {

        this._postsService.getPosts().subscribe(postsData => {this.posts = postsData; this.isLoading = false; console.log(this.posts)}); //סוגריים מסוסלים כי יש כמה פעולות ולא צריך נקודה פסיק בסוף הפעולה האחרונה


       
  }
  

}