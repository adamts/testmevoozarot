import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { UserComponent } from './user/user.component';
import {SpinnerComponent} from './shared/spinner/spinner.component';

import { UsersService } from './users/users.service';
import { PostsService } from './posts/posts.service';
import { ProductsService } from './products/products.service';
import { RouterModule, Routes } from '@angular/router';
import { PostsComponent } from './posts/posts.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { UserFormComponent } from './user-form/user-form.component';
import { AngularFireModule } from 'angularfire2';
import { PostComponent } from './post/post.component';

import { PostFormComponent } from './post-form/post-form.component';
import { ProductsComponent } from './products/products.component';
import { ProductComponent } from './product/product.component';
import { ProductFormComponent } from './product-form/product-form.component';
import { InvoicesComponent } from './invoices/invoices.component';
import { InvoiceFormComponent } from './invoice-form/invoice-form.component'; // for firebase

export const firebaseConfig = {
    apiKey: "AIzaSyBAgGRZfGPGnne4QnDN8JaFP62zdn87Yvs",
    authDomain: "adam1-ff7a3.firebaseapp.com",
    databaseURL: "https://adam1-ff7a3.firebaseio.com",
    storageBucket: "adam1-ff7a3.appspot.com",
    messagingSenderId: "451789403610"
}




const appRoutes: Routes = [

 
  { path: 'invoices', component: InvoicesComponent },
  { path: 'invoice-form', component: InvoiceFormComponent },
 // { path: 'products', component: ProductsComponent },
  //{ path: 'users', component: UsersComponent },
 // { path: 'posts', component: PostsComponent },
  { path: '', component: InvoiceFormComponent },
  { path: '**', component: PageNotFoundComponent }, //thit line need to be last 
];

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    UserComponent,
    SpinnerComponent,
    PostsComponent,
    PageNotFoundComponent,
    UserFormComponent,
    PostComponent,
    PostComponent,
    PostFormComponent,
    ProductComponent,
    ProductsComponent,
    ProductFormComponent,
    InvoicesComponent,
    InvoiceFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseConfig) // for firebase

  ],
  providers: [UsersService,PostsService,ProductsService],
  bootstrap: [AppComponent]
})
export class AppModule { }